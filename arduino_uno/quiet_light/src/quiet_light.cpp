#include "albornolib.hpp"

#define SCALE 1
#define TIME SECOND / 50

#define RED 11
#define GREEN 10
#define BLUE 9

void setup() {
  pinMode(RED, OUTPUT);
  pinMode(GREEN, OUTPUT);
  pinMode(BLUE, OUTPUT);
}

unsigned r = 256;
unsigned g = 0;
unsigned b = 0;

void loop() {
  for (; r > 0; r -= SCALE, g += SCALE) {
    if (r > 200) {
      delay(TIME);
    }
    set(RED, r);
    set(GREEN, g);
    delay(TIME);
  }

  for (; g > 0; g -= SCALE, b += SCALE) {
    if (g > 200) {
      delay(TIME);
    }
    set(GREEN, g);
    set(BLUE, b);
    delay(TIME);
  }

  for (; b > 0; b -= SCALE, r += SCALE) {
    if (b > 200) {
      delay(TIME);
    }
    set(BLUE, b);
    set(RED, r);
    delay(TIME);
  }
}
