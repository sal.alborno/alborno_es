# Esercizi di Sistemi e Reti assegnati il 2023-03-31

## Consegna (parafrasata da voce)

Creare, su un simulatore di circuiti, un circuito con un led RGB che cambia colore in modo regolare usando un Arduino UNO.

## Svolgimento

Normalemente averi usato il simulatore [Tinkercad](https://tinkercad.com/) di Autodesk, ma dopo aver provato il circuito si verificavano seri problemi di performance, dove la simulazione poteva a malapena processare un secondo di logica simulata ogni tre secondi reali (rateo simulazione minore di 33%).  

Ho quindi realizzato il progetto usando [Wokwi](https::/wokwi.com/) il quale è riuscito a simulare il circuito senza alcun intoppo di performance (il rateo di simulazione era stabile tra il 98% e 101%).

### STEP 1: Creare tramite Tinkercad un LED che si spegne ed accende ad intervallo fisso

#### Circuito

![quiet_light/circuit.png](./circuit.png)

#### Codice

##### quiet_light.cpp

```cpp
#include "albornolib.h"

#define SCALE 1
#define TIME SECOND/50

#define RED 11
#define GREEN 10
#define BLUE 9

void setup() {
  pinMode(RED, OUTPUT);
  pinMode(GREEN, OUTPUT);
  pinMode(BLUE, OUTPUT);
}

unsigned r = 256;
unsigned g = 0;
unsigned b = 0;

void loop() {
    for (   ; r > 0; r -= SCALE, g += SCALE) {
        if (r  > 200) { delay(TIME); }
        set(RED, r);
        set(GREEN, g);
        delay(TIME);
    }

    for (   ; g > 0; g -= SCALE, b += SCALE) {
        if (g  > 200) { delay(TIME); }
        set(GREEN, g);
        set(BLUE, b);
        delay(TIME);
    }

    for (   ; b > 0; b -= SCALE, r += SCALE) {
        if (b  > 200) { delay(TIME); }
        set(BLUE, b);
        set(RED, r);
        delay(TIME);
    }
}
```

##### albornolib.h

```cpp
#define SECOND 1000
#define set analogWrite
#define turn digitalWrite
#define ON HIGH
#define OFF LOW
```

## Allegati

[smooth_rgb_light](https://wokwi.com/projects/360796815814818817) - Il progetto su Wokwi
