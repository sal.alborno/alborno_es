#define SECOND 1000
#define ON HIGH
#define OFF LOW
#define turn digitalWrite

#define RED 8
#define GREEN 7

void setup() {
  pinMode(RED, OUTPUT);
  pinMode(GREEN, OUTPUT);
}

void loop() {
  	// Red
	turn(RED, ON);
	turn(GREEN, OFF);
	delay(SECOND*20);

  	// Green
	turn(RED, OFF);
	turn(GREEN, ON);
	delay(SECOND*15);

  	// Yellow
    turn(RED, ON);
	turn(GREEN, ON);
	delay(SECOND*5);
}