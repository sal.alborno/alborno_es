Esercizi di Sistemi e Reti assegnati il 2023-03-18
===============================================

## Consegna
STEP 1:Produrre una relazione sullo studio e creazione tramite Tinkercad dell'accensione e spegnimento di un led utilizzando il codice fornito qui: https://classroom.google.com/u/0/c/MzkxMjM0NTc1MDkz/m/NDQ5MTg4NzIwMjkx/details

La relazione deve contenere il codice commentato (può essere ovviamente modificato), lo schema circuitale estrapolato da Tinkercad e almeno un paragrafo con la spiegazione del lavoro svolto

STEP 2:Produrre una relazione sullo studio e creazione tramite Tinkercad dell'accensione e spegnimento di tre led sincronizzati come in un semaforo con schema di accensione rosso - verde - verde/giallo - rosso - ecc...

La relazione deve contenere il codice commentato, lo schema circuitale estrapolato da Tinkercad e almeno un paragrafo con la spiegazione del lavoro svolto

STEP 3:
Produrre una relazione sullo studio e creazione tramite Tinkercad dell'accensione e spegnimento di sei led sincronizzati come in due semafori con schema di accensione rosso - verde - verde/giallo - rosso - ecc... 

La relazione deve contenere il codice commentato, lo schema circuitale estrapolato da Tinkercad e almeno un paragrafo con la spiegazione del lavoro svolto

IMPORTANTE:
La relazione deve essere realizzata in coppia
Gli STEP 1/2/3 costituiscono le 3 parti della stessa relazione
I semafori devono essere sincronizzati in modo che il rosso e il verde abbiano lo stesso tempo di accensione (modulare adeguatamente la tempistica verde - verde/giallo)
Ogni relazione deve essere consegnata in pdf con il seguente nome Cognome1-Cognome2-LedArduino.pdf e va consegnata da OGNI ALUNNO (un punto in meno per chi non consegna e un punto in meno per il nome errato)
Verrà valutato anche l'ordine (un punto in meno se la relazioni è disordinata e un punto in meno per ogni circuito con fili incrociati )

## Svolgimento
Non ho creato i semafori sincronizzati in modo che il rosso e il verde abbiano lo stesso tempo di accensione poichè renderebbe impossibile completare lo STEP 3.  
Per lo STEP 3 è necessario uno schema di accensione `rosso -> verde -> giallo -> rosso`.  
Perchè il rosso e il verde abbiano lo stesso tempo di accensione è necessario uno schema di accensione `rosso -> giallo -> verde -> giallo -> rosso`.  
Ho fatto in modo che il rosso durasse quanto la durata del verde più la durata del giallo (che da solo dura almeno 3 secondi secondo il Cass. 01/09/2014 n. 18470).

### STEP 1: Creare tramite Tinkercad un LED che si spegne ed accende ad intervallo fisso

#### Circuito
![led_test/circuit.png](./led_test/circuit.png)

#### Codice
Visto che il link dato è rotto ho usato il seguente codice creato da zero:
```cpp
#define SECOND 1000         // -----+
#define ON HIGH             //      |
#define OFF LOW             //      |
#define turn digitalWrite   //      +--- Imposta macro per il preprocessore così da poter usare alias convenienti nel programma senza alcun ritardo causato da subcall
                            //      |
#define LED 2               // -----+

void setup() {              // La funzione `entry point`, agisce come vi fosse una call implicita a `loop()` in fondo
    pinMode(LED, OUTPUT);   // imposta d2 (digital 2) per essere usata per l'output
}

void loop() {               // Questa funzione agisce come vi fosse un loop implicito attorno al suo body (e.g. `while(true) {...}`)
    turn(LED, ON);          // equivalente a `digitalWrite(2, HIGH);` , invia un segnale di circa 5V a d2
	delay(SECOND*5);        // equivalente a `sleep(5000);` fa in modo che il processo continui sensa eseguire azioni per un perido di tempo espresso in millisecondi
    turn(LED, OFF);         // equivalente a `digitalWrite(2, LOW);` , invia un segnale di quasi 0V a d2
    delay(SECOND*5);
}
```

#### Schema circuitale
![led_test/diagram.png](./led_test/diagram.png)

### STEP 2: Creare tre LED sincronizzati come in un semaforo

#### Circuito
![triple_led/circuit.png](./triple_led/circuit.png)

#### Codice
Non aggiungo commenti extra poichè non vi sono nuovi concetti da notare.
```cpp
#define SECOND 1000
#define ON HIGH
#define OFF LOW
#define turn digitalWrite

#define RED 4
#define YELLOW 3
#define GREEN 2

void setup() {
    pinMode(RED, OUTPUT);
    pinMode(YELLOW, OUTPUT);
    pinMode(GREEN, OUTPUT);
}

void loop() {
    // Red
	turn(RED, ON);
	delay(SECOND*20);
    turn(RED, OFF);
  
  	// Green
	turn(GREEN, ON);
	delay(SECOND*15);
    turn(GREEN, OFF);
  
  	// Yellow
    turn(YELLOW, ON);
	delay(SECOND*5);
    turn(YELLOW, OFF);
}
```

#### Schema circuitale
![triple_led/diagram_a.png](./triple_led/diagram_a.png)
![triple_led/diagram_b.png](./triple_led/diagram_b.png)

### STEP 3: Creare due set da tre LED sincronizzati come in due semafori dello stesso incrocio

#### Circuito
![two_triple_led/circuit.png](./two_triple_led/circuit.png)

#### Codice
Non aggiungo commenti extra poichè non vi sono nuovi concetti da notare.
```cpp
#define SECOND 1000
#define ON HIGH
#define OFF LOW
#define turn digitalWrite

#define RED_A 4
#define RED_B 7
#define YELLOW_A 3
#define YELLOW_B 6
#define GREEN_A 2
#define GREEN_B 5

void setup() {
    pinMode(RED_A, OUTPUT);
    pinMode(RED_B, OUTPUT);
    pinMode(YELLOW_A, OUTPUT);
    pinMode(YELLOW_B, OUTPUT);
    pinMode(GREEN_A, OUTPUT);
    pinMode(GREEN_B, OUTPUT);
}

void loop() {
    turn(RED_A, ON);        // -------+
    turn(GREEN_B, ON);      //        |
    delay(SECOND*15);       //        |
    turn(GREEN_B, OFF);     //        |
                            //        +---20s
    turn(YELLOW_B, ON);     //        |
    delay(SECOND*5);        //        |
    turn(RED_A, OFF);       // -------+
    turn(YELLOW_B, OFF);

    turn(RED_B, ON);        // -------+
    turn(GREEN_A, ON);      //        |
    delay(SECOND*15);       //        |
    turn(GREEN_A, OFF);     //        |
                            //        +---20s
    turn(RED_B, ON);        //        |
    turn(YELLOW_A, ON);     //        |
    delay(SECOND*5);        //        |
    turn(RED_B, OFF);       // -------+
    turn(YELLOW_A, OFF);
}
```

#### Schema circuitale
![two_triple_led/diagram_a.png](./two_triple_led/diagram_a.png)
![two_triple_led/diagram_b.png](./two_triple_led/diagram_b.png)

## Extra
Nonostante non fosse stato richiesto dalla consegna ho anche creato un quarto circuito nel quale si utilizza un singolo LED RGB

### Creare un LED RBG che ha un comportamento simile a quello di un semaforo

#### Circuito
![mono_led/circuit.png](./mono_led/circuit.png)

#### Codice
```cpp
#define SECOND 1000
#define ON HIGH
#define OFF LOW
#define turn digitalWrite

#define RED 8
#define GREEN 7

void setup() {
  pinMode(RED, OUTPUT);
  pinMode(GREEN, OUTPUT);
}

void loop() {
  	// Red
	turn(RED, ON);
	turn(GREEN, OFF);
	delay(SECOND*20);

  	// Green
	turn(RED, OFF);
	turn(GREEN, ON);
	delay(SECOND*15);

  	// Yellow
    turn(RED, ON);
	turn(GREEN, ON);
	delay(SECOND*5);
}
```

#### Schema circuitale
![mono_led/diagram.png](./mono_led/diagram.png)

## Allegati
[Semaforo - Arduino UNO](https://www.tinkercad.com/things/9Evj9rAUwSj?sharecode=S2Bd16p6RXptj4_8_-g17ZdwCoqdQAJIYRkbNzgPYng) - Il progetto su Tinkercad