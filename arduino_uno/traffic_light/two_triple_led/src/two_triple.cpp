#define SECOND 1000
#define ON HIGH
#define OFF LOW
#define turn digitalWrite

#define RED_A 4
#define RED_B 7
#define YELLOW_A 3
#define YELLOW_B 6
#define GREEN_A 2
#define GREEN_B 5

void setup() {
    pinMode(RED_A, OUTPUT);
    pinMode(RED_B, OUTPUT);
    pinMode(YELLOW_A, OUTPUT);
    pinMode(YELLOW_B, OUTPUT);
    pinMode(GREEN_A, OUTPUT);
    pinMode(GREEN_B, OUTPUT);
}

void loop() {
	turn(RED_A, ON);        // -------+
    turn(GREEN_B, ON);      //        |
	delay(SECOND*15);       //        |
    turn(GREEN_B, OFF);   	//        |
                            //        +---20s
    turn(YELLOW_B, ON);     //        |
	delay(SECOND*5);        //        |
    turn(RED_A, OFF);       // -------+
    turn(YELLOW_B, OFF);

    turn(RED_B, ON);        // -------+
    turn(GREEN_A, ON);      //        |
	delay(SECOND*15);       //        |
    turn(GREEN_A, OFF);   	//        |
                            //        +---20s
    turn(RED_B, ON);        //        |
    turn(YELLOW_A, ON);     //        |
	delay(SECOND*5);        //        |
    turn(RED_B, OFF);       // -------+
    turn(YELLOW_A, OFF);
}