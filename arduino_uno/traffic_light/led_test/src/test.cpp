#define SECOND 1000
#define ON HIGH
#define OFF LOW
#define turn digitalWrite

#define LED 2

void setup() {
    pinMode(LED, OUTPUT);
}

void loop() {
    turn(LED, ON);
	delay(SECOND*5);
    turn(LED, OFF);
    delay(SECOND*5);
}