#define SECOND 1000
#define ON HIGH
#define OFF LOW
#define turn digitalWrite

#define RED 4
#define YELLOW 3
#define GREEN 2

void setup() {
    pinMode(RED, OUTPUT);
    pinMode(YELLOW, OUTPUT);
    pinMode(GREEN, OUTPUT);
}

void loop() {
    // Red
	turn(RED, ON);
	delay(SECOND*20);
    turn(RED, OFF);
  
  	// Green
	turn(GREEN, ON);
	delay(SECOND*15);
    turn(GREEN, OFF);
  
  	// Yellow
    turn(YELLOW, ON);
	delay(SECOND*5);
    turn(YELLOW, OFF);
}