If not specified, these projects and files are dual-licensed under [Apache 2.0](./LICENSE-APACHE.txt) and [MIT](./LICENSE-MIT.txt) terms.

Some files include explicit copyright notices and/or license notices.
For full authorship information, see the version control history.