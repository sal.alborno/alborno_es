// ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
// ┃ ESP32 io_lcd                                             ┃
// ┃                                                          ┃
// ┃ An example of performing output to a LCD based on user   ┃
// ┃ input (trough pushbuttons) in Rust                       ┃
// ┃                                                          ┃
// ┃ Copyright (c) 2023 Alejandro Alborno,                    ┃
// ┃ Vittoria Concetta Pardu, Hervé Lillaz                    ┃
// ┃ GitLab: https://gitlab.com/sal.alborno/alborno_es        ┃
// ┃ License: Dual license MIT & Apache 2.0                   ┃
// ┃                                                          ┃
// ┃ Credit and thanks to https://wokwi.com/makers/maverick   ┃
// ┃ for the lcd library!                                     ┃
// ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛

#![deny(clippy::all, clippy::nursery, clippy::pedantic)]
use esp_idf_hal::delay::FreeRtos;
use esp_idf_hal::gpio::*;
use esp_idf_hal::i2c::*;
use esp_idf_hal::prelude::*;

mod lcd;

fn main() -> anyhow::Result<()> {
    esp_idf_sys::link_patches();

    let peripherals = Peripherals::take().unwrap();

    let mut buttons_0 = PinDriver::input(peripherals.pins.gpio15)?;
    let mut buttons_1 = PinDriver::input(peripherals.pins.gpio2)?;
    let mut buttons_2 = PinDriver::input(peripherals.pins.gpio4)?;
    let mut buttons_3 = PinDriver::input(peripherals.pins.gpio5)?;

    let sda = peripherals.pins.gpio27;
    let scl = peripherals.pins.gpio26;

    let config = I2cConfig::new().baudrate(100.kHz().into());
    let mut lcd = I2cDriver::new(peripherals.i2c0, sda, scl, &config)?;

    lcd::init(&mut lcd)?;
    lcd::backlight(&mut lcd);

    let mut deco_cycle = 0;
    let mut screen_set = false;

    loop {
        let i_state: [bool; 4] = [
            buttons_0.is_low(),
            buttons_1.is_low(),
            buttons_2.is_low(),
            buttons_3.is_low(),
        ];

        deco_cycle += 1;
        let deco = if deco_cycle < 20 {
            '0'
        } else {
            deco_cycle = 0;
            '-'
        };

        if i_state[0] {
            lcd::set_cursor(&mut lcd, 0, 1);
            lcd::print(&mut lcd, deco);
            lcd::set_cursor(&mut lcd, 19, 1);
            lcd::print(&mut lcd, deco);
            if !screen_set {
                lcd::set_cursor(&mut lcd, 4, 1);
                lcd::print_str(&mut lcd, format!("Hello, World").as_str())
            };
            screen_set = true;
        }
        if i_state[1] {
            lcd::set_cursor(&mut lcd, 0, 1);
            lcd::print(&mut lcd, deco);
            lcd::set_cursor(&mut lcd, 19, 1);
            lcd::print(&mut lcd, deco);
            if !screen_set {
                lcd::set_cursor(&mut lcd, 2, 1);
                lcd::print_str(&mut lcd, format!("Greetings,  Mars").as_str())
            };
            screen_set = true;
        }
        if i_state[2] {
            lcd::set_cursor(&mut lcd, 0, 1);
            lcd::print(&mut lcd, deco);
            lcd::set_cursor(&mut lcd, 19, 1);
            lcd::print(&mut lcd, deco);
            if !screen_set {
                lcd::set_cursor(&mut lcd, 3, 1);
                lcd::print_str(&mut lcd, format!("Hola,  Mercury").as_str())
            };
            screen_set = true;
        }
        if i_state[3] {
            lcd::set_cursor(&mut lcd, 0, 1);
            lcd::print(&mut lcd, deco);
            lcd::set_cursor(&mut lcd, 19, 1);
            lcd::print(&mut lcd, deco);
            if !screen_set {
                lcd::set_cursor(&mut lcd, 4, 1);
                lcd::print_str(&mut lcd, format!("Hey, Jupiter").as_str())
            };
            screen_set = true;
        }

        if !i_state.iter().any(|&x| x) && screen_set {
            lcd::clear(&mut lcd);
            screen_set = false;
        }

        FreeRtos::delay_ms(100);
    }
}
