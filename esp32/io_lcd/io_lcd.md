# Relazione di Sistemi e Reti assegnati il 2023-04-04

## Consegna

Produrre una relazione sullo studio e creazione tramite Tinkercad di un prodotto da voi ideato e progettato seguendo come riferimento il tema assegnato: "GEEK TIME".

E' possibile utilizzare come riferimento i codici che sono stati forniti qui su classroom e quelli dei siti suggeriti o che sono stati trovati in fase di ricerca (vanno ovviamente inseriti nella bibliografia).

La relazione deve contenere il codice adeguatamente commentato, lo schema circuitale estrapolato da Tinkercad, una descrizione esaustiva di almeno una pagina con la spiegazione del lavoro che si intende realizzare e come si colloca all'interno del tema proposto, l'elenco dei componenti necessari per realizzarlo e la bibliografia (completa di link) delle fonti da cui avete attinto per l'ideazione del progetto.

## Svolgimento

### Descrizione progetto 

Il progetto è un “Game Boy”, contente un adattamento digitale del gioco “Hexa” attualmente sotto sviluppo dal professor Gianluca Naccarato.  
Il piano di sviluppo del progetto è diviso in 5 sezioni principali:
1. Input tramite tasti
1. Output tramite LCD
1. Logica del gioco
1. Rifattorizzazione e diminuzione del codice
1. Incapsulazione dell'hardware in un case

Al momento sono state completate le fasi da 1 a 3 e è iniziata la quarta.  

La motivazione per creare un “Game Boy”, è nata grazie ai ricordi dell'infanzia legati a quel sistema di gioco famoso che, per primo, ha permesso di avere a portata di mano l’intrattenimento interattivo a tutti. La particolarità di questo passatempo risiede nell’esistenza di frasi satiriche, che puntano ad abbassare l’autostima del giocatore, quali vengono mostrate sia in caso di perdita sia di vincita. La componente hardware esterna (l'involucro) punta a schernire degli stereotipi e sensibilizzare in maniera ironica sulla dipendenze, presenti nella società attuale. Il significato di questo progetto risiede dietro al concetto di solitudine radicato in una gioventù che si perde in attività futili che finiscono ad isolare la persona.

### Collocazione all'interno del tema "GEEK TIME" 

L'idea del progetto è collegabile alla tematica data, ovvero "Geek Time", perché l'idea di costruire un setup di gioco personalizzato e modificare piattaforme retrò già presenti (come il "Game Boy") è parte integrante della cultura geek. Infatti quest'ultima si caratterizza per una forte presenza di nozioni scientifiche legate al mondo fantasy dei videogiochi, dove ogni azione o decisione sembra possibile.

### Elenco componenti

Componente        | Quantià
----------------- | -------
ESP32             | 1
LCD2004 (pin I2C) | 1
Mini Breadboard   | 1
Tasti             | 4
Cavi              | 15

### Schema circuitale

Il progetto è stato realizzato usando il simulatore [Wokwi](https:/wokwi.com/) per vari motivi tra cui: compilazione veloce, simulazione stabile, supporto per Rust sull'ESP32 e modifica testuale di diagramma.  
Al momento Wokwi non permette l'estrapolazione di uno schema circuitale standard, è stata allegata un'immagine del circuito.

### Circuito

![io_lcd/circuit.png](./circuit.png)

### Codice commentato

```rust
// librerie

fn main() -> anyhow::Result<()> {
    // variabili per ineragire con altri dispositivi
    let peripherals = Peripherals::take().unwrap();
    let mut buttons_0 = PinDriver::input(peripherals.pins.gpio15)?;
    let mut buttons_1 = PinDriver::input(peripherals.pins.gpio2)?;
    let mut buttons_2 = PinDriver::input(peripherals.pins.gpio4)?;
    let mut buttons_3 = PinDriver::input(peripherals.pins.gpio5)?;
    let sda = peripherals.pins.gpio27;
    let scl = peripherals.pins.gpio26;
    let config = I2cConfig::new().baudrate(100.kHz().into());
    let mut lcd = I2cDriver::new(peripherals.i2c0, sda, scl, &config)?;

    // inizializzazione del display
    lcd::init(&mut lcd)?;
    lcd::backlight(&mut lcd);

    // variabili da mantenere tra cicli
    let mut deco_cycle = 0;
    let mut screen_set = false;

    loop {
        // raccolta dello stato attuale dei tasti
        let i_state: [bool; 4] = [
            buttons_0.is_low(),
            buttons_1.is_low(),
            buttons_2.is_low(),
            buttons_3.is_low(),
        ];

        // impostazione frame attuale della animazione degli occhi
        deco_cycle += 1;
        let deco = if deco_cycle < 20 {
            '0'
        } else {
            deco_cycle = 0;
            '-'
        };

        // stampa a schermo di una frase in base al tasto premuto
        if i_state[0] {
            lcd::set_cursor(&mut lcd, 0, 1);
            lcd::print(&mut lcd, deco);
            lcd::set_cursor(&mut lcd, 19, 1);
            lcd::print(&mut lcd, deco);
            if !screen_set {
                lcd::set_cursor(&mut lcd, 4, 1);
                lcd::print_str(&mut lcd, format!("Hello, World").as_str())
            };
            screen_set = true;
        }
        if i_state[1] {
            lcd::set_cursor(&mut lcd, 0, 1);
            lcd::print(&mut lcd, deco);
            lcd::set_cursor(&mut lcd, 19, 1);
            lcd::print(&mut lcd, deco);
            if !screen_set {
                lcd::set_cursor(&mut lcd, 2, 1);
                lcd::print_str(&mut lcd, format!("Greetings,  Mars").as_str())
            };
            screen_set = true;
        }
        if i_state[2] {
            lcd::set_cursor(&mut lcd, 0, 1);
            lcd::print(&mut lcd, deco);
            lcd::set_cursor(&mut lcd, 19, 1);
            lcd::print(&mut lcd, deco);
            if !screen_set {
                lcd::set_cursor(&mut lcd, 3, 1);
                lcd::print_str(&mut lcd, format!("Hola,  Mercury").as_str())
            };
            screen_set = true;
        }
        if i_state[3] {
            lcd::set_cursor(&mut lcd, 0, 1);
            lcd::print(&mut lcd, deco);
            lcd::set_cursor(&mut lcd, 19, 1);
            lcd::print(&mut lcd, deco);
            if !screen_set {
                lcd::set_cursor(&mut lcd, 4, 1);
                lcd::print_str(&mut lcd, format!("Hey, Jupiter").as_str())
            };
            screen_set = true;
        }

        // pulitura dello schermo quando necessario
        if !i_state.iter().any(|&x| x) && screen_set {
            lcd::clear(&mut lcd);
            screen_set = false;
        }

        // limitazione dell'esecuzione del loop ad una volta per decimo di secondo
        FreeRtos::delay_ms(100);
    }
}
```

## Allegati

[io_lcd](https://wokwi.com/projects/365007446095825921) - Il progetto su Wokwi
