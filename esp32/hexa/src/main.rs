// ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
// ┃ ESP32 Hexa                                               ┃
// ┃                                                          ┃
// ┃ A simple dice based game on esp32                        ┃
// ┃                                                          ┃
// ┃                                                          ┃
// ┃ Copyright (c) 2023 Alejandro Alborno,                    ┃
// ┃ Vittoria Concetta Pardu, Hervé Lillaz                    ┃
// ┃ GitLab: https://gitlab.com/sal.alborno/alborno_es        ┃
// ┃ License: Dual license MIT & Apache 2.0                   ┃
// ┃                                                          ┃
// ┃ Credit and thanks to https://wokwi.com/makers/maverick   ┃
// ┃ for the lcd library!                                     ┃
// ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛

#![deny(
    clippy::all,
    clippy::nursery,
    clippy::pedantic,
    clippy::cargo,
    warnings
)]
mod esp;
use esp::{await_press, delay_or_press, print, startup_screen, uin_u8, System};
mod game;
use game::{check_winner, make_players, process_round, Id};

fn main() {
    let mut sys = System::setup();
    loop {
        startup_screen(&mut sys.lcd);
        await_press(&mut sys);
        let mut players = make_players(uin_u8(&mut sys, "How many will play?", 2, 8).into());
        let mut winner_id: Option<Id> = None;
        let mut round = 0u8;
        while {
            round += 1;
            check_winner(&players).map_or(true, |v| {
                winner_id = Some(v);
                false
            })
        } {
            print(&mut sys.lcd, format!("- Round {round}").as_str());
            delay_or_press(&mut sys, 2500);

            process_round(&mut sys, &mut players);
        }
        print(
            &mut sys.lcd,
            format!("Player {:?} won!", winner_id.unwrap()).as_str(),
        );
        await_press(&mut sys);
    }
}
