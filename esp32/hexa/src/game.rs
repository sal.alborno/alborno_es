use crate::esp::{await_press, delay_or_press, lcd, print, uin_bool, System};
use esp_idf_hal::delay::FreeRtos;
use esp_idf_hal::i2c::I2cDriver;
use std::fmt;
pub mod dice;
use dice::{to_instances, Combination, Die, Roll};

#[derive(Clone)]
pub struct Player {
    pub id: Id,
    pub trophies: Vec<Die>,
    pub dice: Vec<Die>,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum Id {
    Alfa,
    Bravo,
    Carl,
    Delta,
    Echo,
    Fox,
    Golf,
    Hotel,
}

impl fmt::Display for Id {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        f.pad(&format!("{self:?}"))
    }
}

const PLAYERS: [Id; 8] = [
    Id::Alfa,
    Id::Bravo,
    Id::Carl,
    Id::Delta,
    Id::Echo,
    Id::Fox,
    Id::Golf,
    Id::Hotel,
];

pub fn make_players(u: usize) -> Vec<Player> {
    let mut v = Vec::with_capacity(u);
    for i in PLAYERS.iter().take(u) {
        v.push(Player {
            id: i.clone(),
            trophies: Vec::new(),
            dice: vec![Die::new(); 6],
        });
    }
    v
}

pub fn process_round(sys: &mut System, ps: &mut [Player]) {
    let reroll_count = 2;
    let mut someone_wants_reroll = true;
    for player in ps.iter_mut() {
        player.dice.roll();
        display_dice_roll(&mut sys.lcd, player);
        await_press(sys);
    }
    for i in 0..reroll_count {
        if !someone_wants_reroll {
            break;
        }
        someone_wants_reroll = false;
        for player in ps.iter_mut() {
            if uin_bool(sys, format!("is {:?} rerolling?", player.id).as_str()) {
                print(&mut sys.lcd, "Aight!");
                delay_or_press(sys, 1750);
                player.dice.roll();
                someone_wants_reroll = true;
            }
        }
        if i != reroll_count && someone_wants_reroll {
            for player in ps.iter_mut() {
                display_dice_roll(&mut sys.lcd, player);
                await_press(sys);
            }
        }
    }
    let mut winner = 0;
    for i in 0..ps.len() {
        if Combination::from_instances(to_instances(&ps[winner].dice))
            < Combination::from_instances(to_instances(&ps[i].dice))
        {
            winner = i;
        }
    }
    let mut winners = 0;
    for i in 0..ps.len() {
        if Combination::from_instances(to_instances(&ps[winner].dice))
            == Combination::from_instances(to_instances(&ps[i].dice))
        {
            winners += 1;
        }
    }
    if winners > 1 {
        print(&mut sys.lcd, "It is a tie!");
        delay_or_press(sys, 1750);
        return;
    }
    let winner = &mut ps[winner];
    print(
        &mut sys.lcd,
        format!("{} won the round", winner.id).as_str(),
    );
    delay_or_press(sys, 1750);

    if !winner.trophies.is_empty() {
        display_trophies(&mut sys.lcd, winner);
        await_press(sys);
    }
    let index = choose_trophy(sys, winner);
    print(&mut sys.lcd, format!("+ {}", winner.dice[index]).as_str());
    delay_or_press(sys, 1750);
    winner.trophies.push(winner.dice.remove(index));
}

fn display_dice_roll(lcd: &mut I2cDriver, p: &Player) {
    let combination = Combination::from_instances(to_instances(&p.dice));
    let dice = {
        format!(
            "  {}  ",
            p.dice
                .iter()
                .enumerate()
                .flat_map(|(i, d)| {
                    let delimiter = match i {
                        2 => "     ",
                        5 => "",
                        _ => "   ",
                    };
                    vec![d.to_string(), delimiter.to_string()]
                })
                .collect::<String>()
        )
    };
    print(
        lcd,
        format!(
            "{:<20}{dice:<40}{combination:?}",
            format!("{}'s roll:", p.id)
        )
        .as_str(),
    );
}

fn display_trophies(lcd: &mut I2cDriver, p: &mut Player) {
    let trophies = {
        format!(
            "     {}     ",
            p.trophies
                .iter()
                .enumerate()
                .flat_map(|(i, d)| {
                    let delimiter = match i {
                        1 => "          ",
                        3 => "",
                        _ => "    ",
                    };
                    vec![d.to_string(), delimiter.to_string()]
                })
                .collect::<String>()
        )
    };

    print(
        lcd,
        format!("{:<20}{trophies:<40}", format!("{}'s trophies:", p.id)).as_str(),
    );
}

// TODO: improve visual selection
const HORIZONTAL_INDICATOR: char = '>';
fn choose_trophy(sys: &mut System, p: &mut Player) -> usize {
    let dice = {
        format!(
            "  {}  ",
            p.dice
                .iter()
                .enumerate()
                .flat_map(|(i, d)| {
                    let delimiter = match i {
                        2 => "     ",
                        5 => "",
                        _ => "   ",
                    };
                    vec![d.to_string(), delimiter.to_string()]
                })
                .collect::<String>()
        )
    };
    print(
        &mut sys.lcd,
        format!("{:<20}{dice:<40}", "Choose a trophy:").as_str(),
    );
    lcd::set_cursor(&mut sys.lcd, 1, 1);
    lcd::print(&mut sys.lcd, HORIZONTAL_INDICATOR);
    let mut choice = 0;
    loop {
        if sys.button_0.is_low() {
            lcd::set_cursor(&mut sys.lcd, 19, 3);
            lcd::print(&mut sys.lcd, 'X');
            while sys.button_0.is_low() {
                FreeRtos::delay_ms(16);
            }
            lcd::clear(&mut sys.lcd);
            break choice;
        }
        if sys.button_1.is_low() {
            lcd::set_cursor(&mut sys.lcd, 19, 3);
            lcd::print(&mut sys.lcd, 'X');
            while sys.button_1.is_low() {
                FreeRtos::delay_ms(16);
            }
            lcd::set_cursor(&mut sys.lcd, 19, 3);
            lcd::print(&mut sys.lcd, ' ');

            lcd::set_cursor(
                &mut sys.lcd,
                (choice % 3 * 6 + 1).try_into().unwrap(),
                if choice > 2 { 2 } else { 1 },
            );
            lcd::print(&mut sys.lcd, ' ');
            choice = (p.dice.len() + choice - 1) % p.dice.len();
            lcd::set_cursor(
                &mut sys.lcd,
                (choice % 3 * 6 + 1).try_into().unwrap(),
                if choice > 2 { 2 } else { 1 },
            );
            lcd::print(&mut sys.lcd, HORIZONTAL_INDICATOR);
        };
        if sys.button_2.is_low() {
            lcd::set_cursor(&mut sys.lcd, 19, 3);
            lcd::print(&mut sys.lcd, 'X');
            while sys.button_2.is_low() {
                FreeRtos::delay_ms(16);
            }
            lcd::set_cursor(&mut sys.lcd, 19, 3);
            lcd::print(&mut sys.lcd, ' ');

            lcd::set_cursor(
                &mut sys.lcd,
                (choice % 3 * 6 + 1).try_into().unwrap(),
                if choice > 2 { 2 } else { 1 },
            );
            lcd::print(&mut sys.lcd, ' ');
            choice = (choice + 1) % p.dice.len();
            lcd::set_cursor(
                &mut sys.lcd,
                (choice % 3 * 6 + 1).try_into().unwrap(),
                if choice > 2 { 2 } else { 1 },
            );
            lcd::print(&mut sys.lcd, HORIZONTAL_INDICATOR);
        };
    }
}

pub fn check_winner(ps: &Vec<Player>) -> Option<Id> {
    for player in ps {
        match Combination::from_instances(to_instances(&player.trophies)) {
            Combination::None | Combination::MonoPair => continue,
            _ => return Some(player.id.clone()),
        };
    }
    None
}
