#[allow(clippy::all, warnings)] // just for this external module
pub mod lcd;
use esp_idf_hal::delay::FreeRtos;
use esp_idf_hal::gpio::{Gpio2, Gpio23, Gpio4, Input, PinDriver};
use esp_idf_hal::i2c::{I2cConfig, I2cDriver};
use esp_idf_hal::prelude::*;

pub struct System<'a> {
    pub button_0: PinDriver<'a, Gpio2, Input>,
    pub button_1: PinDriver<'a, Gpio4, Input>,
    pub button_2: PinDriver<'a, Gpio23, Input>,
    pub lcd: I2cDriver<'a>,
}

impl System<'_> {
    pub fn setup() -> System<'static> {
        static mut CALLED: bool = false;
        unsafe {
            assert!(!CALLED);
            CALLED = !CALLED;
        }
        esp_idf_hal::sys::link_patches();
        let peripherals = Peripherals::take().unwrap();
        let sda = peripherals.pins.gpio27;
        let scl = peripherals.pins.gpio26;
        let config = I2cConfig::new().baudrate(100.kHz().into());
        let mut sys = System {
            button_0: PinDriver::input(peripherals.pins.gpio2).unwrap(),
            button_1: PinDriver::input(peripherals.pins.gpio4).unwrap(),
            button_2: PinDriver::input(peripherals.pins.gpio23).unwrap(),
            lcd: I2cDriver::new(peripherals.i2c0, sda, scl, &config).unwrap(),
        };
        lcd::init(&mut sys.lcd).unwrap();
        lcd::backlight(&mut sys.lcd);
        sys
    }
}

pub fn startup_screen(lcd: &mut I2cDriver) {
    lcd::set_cursor(lcd, 0, 0);
    lcd::print_str(lcd, " ___ ___ __ __ ___v1");
    lcd::set_cursor(lcd, 0, 1);
    lcd::print_str(lcd, "| | | __|  |  | _ |");
    lcd::set_cursor(lcd, 0, 2);
    lcd::print_str(lcd, "|   | __|-   -|   |");
    lcd::set_cursor(lcd, 0, 3);
    lcd::print_str(lcd, "|_|_|___|__|__|_|_|");
}

pub fn print(lcd: &mut I2cDriver, s: &str) {
    lcd::clear(lcd);
    lcd::set_cursor(lcd, 0, 0);
    match s.len() {
        l if l < 20 => {
            lcd::print_str(lcd, s);
        }
        l if l < 40 => {
            lcd::print_str(lcd, &s[0..20]);
            lcd::set_cursor(lcd, 0, 1);
            lcd::print_str(lcd, &s[20..]);
        }
        l if l < 60 => {
            lcd::print_str(lcd, &s[0..20]);
            lcd::set_cursor(lcd, 0, 1);
            lcd::print_str(lcd, &s[20..40]);
            lcd::set_cursor(lcd, 0, 2);
            lcd::print_str(lcd, &s[40..]);
        }
        l if l < 80 => {
            lcd::print_str(lcd, &s[0..20]);
            lcd::set_cursor(lcd, 0, 1);
            lcd::print_str(lcd, &s[20..40]);
            lcd::set_cursor(lcd, 0, 2);
            lcd::print_str(lcd, &s[40..60]);
            lcd::set_cursor(lcd, 0, 3);
            lcd::print_str(lcd, &s[60..]);
        }
        _ => panic!("Tried to print string longer than 80 bytes"),
    }
}

pub fn await_press(sys: &mut System) {
    let mut time = 0u8;
    while sys.button_0.is_high() {
        FreeRtos::delay_ms(16);
        time += 1;
        if time == 50 {
            lcd::set_cursor(&mut sys.lcd, 19, 3);
            lcd::print(&mut sys.lcd, '_');
        }
        if time == 100 {
            lcd::set_cursor(&mut sys.lcd, 19, 3);
            lcd::print(&mut sys.lcd, ' ');
            time = 0;
        }
    }
    lcd::set_cursor(&mut sys.lcd, 19, 3);
    lcd::print(&mut sys.lcd, 'X');
    while sys.button_0.is_low() {
        FreeRtos::delay_ms(16);
    }
}
pub fn delay_or_press(sys: &mut System, ms: u32) {
    let mut time = 0u8;
    let mut pressed = false;
    while u32::from(time) * 16 < ms {
        FreeRtos::delay_ms(16);
        time += 1;
        if sys.button_0.is_low() {
            lcd::set_cursor(&mut sys.lcd, 19, 3);
            lcd::print(&mut sys.lcd, 'X');
            pressed = true;
        }
        if sys.button_0.is_high() && pressed {
            break;
        }
    }
}

const HORIZONTAL_INDICATOR: char = '>';
pub fn uin_bool(sys: &mut System, msg: &str) -> bool {
    let mut choice = false;
    lcd::clear(&mut sys.lcd);
    lcd::set_cursor(&mut sys.lcd, 0, 0);
    lcd::print_str(&mut sys.lcd, msg);
    lcd::set_cursor(&mut sys.lcd, 2, 1);
    lcd::print_str(&mut sys.lcd, "no");
    lcd::set_cursor(&mut sys.lcd, 2, 2);
    lcd::print_str(&mut sys.lcd, "yes");
    lcd::set_cursor(&mut sys.lcd, 0, 1 + usize::from(choice));
    lcd::print(&mut sys.lcd, HORIZONTAL_INDICATOR);

    loop {
        FreeRtos::delay_ms(16);
        if sys.button_0.is_low() {
            lcd::set_cursor(&mut sys.lcd, 19, 3);
            lcd::print(&mut sys.lcd, 'X');
            while sys.button_0.is_low() {
                FreeRtos::delay_ms(16);
            }
            lcd::clear(&mut sys.lcd);
            break choice;
        }
        if sys.button_1.is_low() || sys.button_2.is_low() {
            lcd::set_cursor(&mut sys.lcd, 19, 3);
            lcd::print(&mut sys.lcd, 'X');
            while sys.button_1.is_low() || sys.button_2.is_low() {
                FreeRtos::delay_ms(16);
            }
            lcd::set_cursor(&mut sys.lcd, 19, 3);
            lcd::print(&mut sys.lcd, ' ');

            choice = !choice;
            lcd::set_cursor(&mut sys.lcd, 0, 2 - usize::from(choice));
            lcd::print(&mut sys.lcd, ' ');
            lcd::set_cursor(&mut sys.lcd, 0, 1 + usize::from(choice));
            lcd::print(&mut sys.lcd, HORIZONTAL_INDICATOR);
        };
    }
}

pub fn uin_u8(sys: &mut System, msg: &str, min: u8, max: u8) -> u8 {
    let mut choice = min;
    lcd::clear(&mut sys.lcd);
    lcd::set_cursor(&mut sys.lcd, 0, 0);
    lcd::print_str(&mut sys.lcd, msg);
    lcd::set_cursor(&mut sys.lcd, 0, 2);
    lcd::print_str(
        &mut sys.lcd,
        format!("{:>10}", format!("{HORIZONTAL_INDICATOR} {choice}")).as_str(),
    );

    loop {
        FreeRtos::delay_ms(16);
        if sys.button_0.is_low() {
            lcd::set_cursor(&mut sys.lcd, 19, 3);
            lcd::print(&mut sys.lcd, 'X');
            while sys.button_0.is_low() {
                FreeRtos::delay_ms(16);
            }
            lcd::clear(&mut sys.lcd);
            break choice;
        }
        if sys.button_2.is_low() {
            lcd::set_cursor(&mut sys.lcd, 19, 3);
            lcd::print(&mut sys.lcd, 'X');
            while sys.button_2.is_low() {
                FreeRtos::delay_ms(16);
            }
            lcd::set_cursor(&mut sys.lcd, 19, 3);
            lcd::print(&mut sys.lcd, ' ');

            if choice == max {
                choice = min
            } else {
                choice += 1
            }
            lcd::set_cursor(&mut sys.lcd, 0, 2);
            lcd::print_str(
                &mut sys.lcd,
                format!("{:>10}", format!("{HORIZONTAL_INDICATOR} {choice}")).as_str(),
            );
        };
        if sys.button_1.is_low() {
            lcd::set_cursor(&mut sys.lcd, 19, 3);
            lcd::print(&mut sys.lcd, 'X');
            while sys.button_1.is_low() {
                FreeRtos::delay_ms(16);
            }
            lcd::set_cursor(&mut sys.lcd, 19, 3);
            lcd::print(&mut sys.lcd, ' ');

            if choice == min {
                choice = max
            } else {
                choice -= 1
            }
            lcd::set_cursor(&mut sys.lcd, 0, 2);
            lcd::print_str(
                &mut sys.lcd,
                format!("{:>10}", format!("{HORIZONTAL_INDICATOR} {choice}")).as_str(),
            );
        }
    }
}
