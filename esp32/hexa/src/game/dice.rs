use std::collections::HashMap;
use std::fmt;

pub trait Roll {
    fn roll(&mut self);
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Die {
    pub val: u8,
}

impl Die {
    pub const fn new() -> Self {
        Self { val: 0 }
    }
}

impl Roll for Die {
    #[allow(clippy::cast_possible_truncation)]
    fn roll(&mut self) {
        unsafe {
            self.val = esp_idf_hal::sys::esp_random() as u8 % 6 + 1;
        }
    }
}

impl fmt::Display for Die {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        write!(f, "[{}]", self.val)
    }
}

impl Roll for Vec<Die> {
    fn roll(&mut self) {
        for die in self {
            die.roll();
        }
    }
}

pub fn to_instances(dice: &Vec<Die>) -> HashMap<Die, u8> {
    let mut instances = HashMap::new();
    for v in dice {
        *instances.entry(v.clone()).or_insert(0) += 1;
    }
    instances
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum Combination {
    None,
    MonoPair,
    DoublePair,
    MonoTris,
    TripleScala,
    Full, // MonoPair + Tris
    Tetra,
    TetraScala,
    Penta,
    TriplePair,
    DoubleTris,
    SuperFull, // MonoPair + Tetra
    PentaScala,
    Hexe, // HextupleScala
}

impl Combination {
    pub fn from_instances(i: HashMap<Die, u8>) -> Self {
        #[allow(clippy::enum_glob_use)]
        use Combination::*;
        let (mut k, mut v): (Vec<_>, Vec<_>) = (i.keys().collect(), i.values().collect());
        k.sort();
        v.sort();
        let (k, v): (Vec<_>, Vec<_>) =
            (k.into_iter().rev().collect(), v.into_iter().rev().collect());

        let possible_scala = match k.as_slice() {
            sli if sli.consecutive_for(5) => PentaScala,
            sli if sli.consecutive_for(4) => TetraScala,
            sli if sli.consecutive_for(3) => TripleScala,
            _ => None,
        };

        match v.as_slice() {
            [6, ..] => Hexe,
            _ if possible_scala == PentaScala => PentaScala,
            [4, 2, ..] => SuperFull,
            [3, 3, ..] => DoubleTris,
            [2, 2, 2, ..] => TriplePair,
            [5, ..] => Penta,
            _ if possible_scala == TetraScala => TetraScala,
            [4, ..] => Tetra,
            [3, 2, ..] => Full,
            _ if possible_scala == TripleScala => TripleScala,
            [3, ..] => MonoTris,
            [2, 2, ..] => DoublePair,
            [2, ..] => MonoPair,
            _ => None,
        }
    }
}

trait Consecutive {
    fn consecutive_for(&self, u: usize) -> bool;
}

impl Consecutive for &[&Die] {
    fn consecutive_for(&self, u: usize) -> bool {
        self.len() >= u
            && ({
                let mut consecutive = 0;
                for i in 0..self.len() - 1 {
                    if self[i].val == self[i + 1].val + 1 {
                        consecutive += 1;
                    } else {
                        consecutive = 0;
                    };
                }
                consecutive == u - 1
            } || {
                let mut consecutive = 0;
                for i in 0..self.len() - 1 {
                    if self[i].val == self[i + 1].val - 1 {
                        consecutive += 1;
                    } else {
                        consecutive = 0;
                    };
                }
                consecutive == u - 1
            })
    }
}

#[test]
fn dice_ord() {
    use Combination::{DoubleTris, Hexe, Tetra, TripleScala};
    assert!(Tetra > TripleScala);
    assert!(DoubleTris < Hexe);
}

#[test]
fn correct_combination_recognition() {
    assert_eq!(
        Combination::from_instances(to_instances(&vec![
            Die { val: 5 },
            Die { val: 4 },
            Die { val: 5 },
            Die { val: 4 },
            Die { val: 3 },
            Die { val: 6 }
        ])),
        Combination::TetraScala
    );
}
